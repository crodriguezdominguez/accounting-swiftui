//
//  NewYearlyAccountingView.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 10/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct NewYearlyAccountingView: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @State var yearString: String = ""
    
    let action: (Int?) -> Void
    
    var body: some View {
        VStack(alignment: .center) {
            Text("Add new accounting year")
                .font(.title)
            
            Text("Please, enter a new accounting year")
                .font(.body)
            
            TextField("New accounting year", text: $yearString)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.vertical, 15)
            
            HStack {
                Spacer()
                
                Button(action: {
                    self.action(nil)
                }) { Text("Cancel").bold().font(.system(size: 20.0)) }
                
                Spacer()
                
                Divider().frame(height: 40.0, alignment: .center)
                
                Spacer()
                
                Button(action: {
                    self.action(Int(self.yearString))
                }) { Text("Add").font(.system(size: 20.0)) }
                
                Spacer()
            }
        }
        .padding(.all)
        .background(colorScheme == ColorScheme.light ? Color.white : Color(white: 0.1))
        .cornerRadius(20.0)
    }
}

struct NewYearlyAccountingView_Previews: PreviewProvider {
    static var previews: some View {
        NewYearlyAccountingView { (_) in
            
        }
    }
}
