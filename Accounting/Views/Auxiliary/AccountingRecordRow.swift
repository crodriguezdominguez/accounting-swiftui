//
//  AccountingRecordRow.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 14/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct AccountingRecordRow: View {
    let accountingRecord: AccountingRecord
    
    var body: some View {
        HStack {
            Text(accountingRecord.description)
            
            Spacer()
            
            PriceText(value: accountingRecord)
        }
    }
}

struct ApunteContableRow_Previews: PreviewProvider {
    private static var records = [
        AccountingRecord(price: 30.0, description: "Water", type: .expense),
        AccountingRecord(price: 20.0, description: "Jeans", type: .profit)
    ]
    
    static var previews: some View {
        Group {
            List {
                ForEach(records) { item in
                    AccountingRecordRow(accountingRecord: item)
                }
            }.colorScheme(.light)
            
            List {
                ForEach(records) { item in
                    AccountingRecordRow(accountingRecord: item)
                }
            }.colorScheme(.dark)
        }
        
    }
}
