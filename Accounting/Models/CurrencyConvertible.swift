//
//  CurrencyConvertible.swift
//  Contabilidad
//
//  Created by Carlos Rodríguez Domínguez on 14/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import Foundation

protocol CurrencyConvertible : CustomStringConvertible {
    var total: Float {get}
}

extension CurrencyConvertible {
    var formattedTotal: String {
        NumberFormatter.localizedString(from: NSNumber(value: self.total), number: .currency)
    }
    
    var description: String {
        formattedTotal
    }
}

