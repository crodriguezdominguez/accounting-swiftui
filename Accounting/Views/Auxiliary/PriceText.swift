//
//  PriceText.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 14/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct PriceText<T : CurrencyConvertible>: View {
    let value: T
    
    var body: some View {
        Text(value.formattedTotal)
            .foregroundColor(value.total < 0 ? .red : nil)
            .bold()
    }
}

struct PriceText_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            VStack {
                PriceText(value: AccountingRecord(price: 5.0, description: "", type: .profit))
                
                PriceText(value: AccountingRecord(price: 5.0, description: "", type: .expense))
            }
        }
    }
}
