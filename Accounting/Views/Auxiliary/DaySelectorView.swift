//
//  DaySelectorView.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 14/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct DaySelectorView: View {
    @EnvironmentObject var repository: AccountingRepository
    
    @Binding var selectedDay: Int
        
    let year: Int
    let month: Month
    
    private var monthlyAccounting: MonthlyAccounting {
        repository.monthlyAccounting(year: year, month: month)
    }
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: true) {
            HStack(alignment: .center, spacing: 0.0) {
                ForEach(Array(monthlyAccounting.dailyAccountings.enumerated()), id: \.1.day) { idx, dailyAccounting in
                    DailyAccountingRow(dailyAccounting: dailyAccounting)
                        .disabled(idx == self.selectedDay)
                        .onTapGesture {
                            self.selectedDay = idx
                        }
                }
            }
        }
        .frame(height: 80.0)
    }
}

struct DaySelectorView_Previews : PreviewProvider {
    @State static var day = 0
    
    static var previews: some View {
        Group {
            DaySelectorView(selectedDay: $day, year: 2019, month: .january)
                .environmentObject(AccountingRepository.previewData)
        }
    }
}
