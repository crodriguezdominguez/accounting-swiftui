//
//  DailyAccounting.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 08/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import Foundation

struct DailyAccounting: Codable, CurrencyConvertible {
    let day: Int
    var accountingRecords: [AccountingRecord]
    
    init(day: Int, accountingRecords: [AccountingRecord] = []) {
        self.day = day
        self.accountingRecords = accountingRecords
    }
    
    var total: Float {
        accountingRecords.reduce(0.0) { (total, apunte) -> Float in
            total + (apunte.type == .expense ? -apunte.price : apunte.price)
        }
    }
}
