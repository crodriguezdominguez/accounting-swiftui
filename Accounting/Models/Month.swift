//
//  Month.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 16/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import Foundation

enum Month: Int, CaseIterable, CustomStringConvertible, Codable {
    case january, february, march, april, may, june, july, august, september, october, november, december
    
    func countDays(year: Int) -> Int {
        let calendar = Calendar.current
        let dateComponents = DateComponents(year: year, month: self.rawValue+1)
        
        return calendar.range(of: .day, in: .month, for: calendar.date(from: dateComponents)!)!.count
    }
    
    var next: Month {
        Month(rawValue: self.rawValue+1 % Month.allCases.count)!
    }
    
    var previous: Month {
        if self == .january {
            return .december
        }
        else{
            return Month(rawValue: self.rawValue-1)!
        }
    }
    
    var description: String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        return formatter.standaloneMonthSymbols[self.rawValue]
    }
}
