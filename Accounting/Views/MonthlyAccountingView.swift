//
//  MonthlyAccountingView.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 08/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct MonthlyAccountingView: View {
    @EnvironmentObject var repository: AccountingRepository
    
    @State var selectedDay: Int = 0
    @State var showAddItem = false
    
    let year: Int
    let month: Month
    
    var formattedDate: String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .ordinal
        let dayString = numberFormatter.string(from: NSNumber(value: selectedDay+1))!
        
        return "\(dayString) \(month.description)"
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            DaySelectorView(selectedDay: $selectedDay, year: year, month: month)
            
            AccountingRecordsListView(selectedDay: $selectedDay, year: year, month: month)
        }
        .sheet(isPresented: $showAddItem) {
            NewAccountingRecordView(action: self.onNewAccountingRecord)
        }
        .navigationBarTitle(formattedDate)
        .navigationBarItems(
            trailing: Button(action: toggleAddItem, label: {
                    Image(systemName: "plus")
                })
        )
        
    }
    
    func onNewAccountingRecord(record: AccountingRecord?) {
        if let record = record {
            self.repository.append(accountingRecord: record, year: self.year, month: self.month, day: self.selectedDay)
        }
        
        self.toggleAddItem()
    }
    
    func toggleAddItem() {
        self.showAddItem.toggle()
    }
}

struct MonthlyAccountingView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            NavigationView {
                MonthlyAccountingView(year: 2019, month: .february)
                    .environmentObject(AccountingRepository.previewData)
            }
        }
    }
}
