//
//  AccountingRecord.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 16/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import Foundation

struct AccountingRecord: Codable, Identifiable, CurrencyConvertible, CustomStringConvertible {
    let id = UUID()
    let price: Float
    let description: String
    let type: RecordType
    
    enum RecordType: String, Codable, CaseIterable, CustomStringConvertible {
        case expense, profit
        
        var description: String {
            self.rawValue.prefix(1).uppercased() + self.rawValue.dropFirst()
        }
    }
    
    init(price: Float, description: String, type: RecordType) {
        self.price = price
        self.description = description
        self.type = type
    }
    
    var total: Float {
        let product: Float = self.type == .expense ? -1.0 : 1.0
        return product * self.price
    }
}
