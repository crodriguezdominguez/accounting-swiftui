//
//  DailyAccountingRow.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 08/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct DailyAccountingRow: View {
    let dailyAccounting: DailyAccounting
        
    var body: some View {
        VStack {
            Text(self.dailyAccounting.day.description)
                .bold()
                .padding()
                .background(Color.red)
                .foregroundColor(.white)
                .clipShape(Circle())
            
            Spacer()
            
            PriceText(value: self.dailyAccounting)
        }
        .frame(width: 80.0, height: 80.0)
    }
}

struct DailyAccountingRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            DailyAccountingRow(dailyAccounting: DailyAccounting(day: 23))
        }
    }
}
