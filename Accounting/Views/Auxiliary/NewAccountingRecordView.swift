//
//  NewAccountingRecordView.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 15/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct NewAccountingRecordView: View {
    @State var description: String = ""
    @State var price: Float = 0.0
    @State var type: AccountingRecord.RecordType = .profit
    
    let action: (AccountingRecord?) -> Void
    
    private var currencyFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        return formatter
    }
    
    var body: some View {
        NavigationView {
            Form {
                TextField("Enter a description", text: $description)
                
                TextField("Enter a price", value: $price, formatter: currencyFormatter)
                
                Picker(selection: $type, label: Text("Type")) {
                    ForEach(AccountingRecord.RecordType.allCases, id: \.rawValue) {
                        Text($0.description).tag($0)
                    }
                }
                .pickerStyle(WheelPickerStyle())
                .frame(height: 150.0)
            }
            .navigationBarTitle("New Record")
            .navigationBarItems(leading: Button(action: self.onCancel) {
                Text("Cancel")
                    .bold()
            }, trailing: Button(action: self.onAdd) {
                Text("Add")
            })
        }
    }
    
    func onCancel() {
        self.action(nil)
    }
    
    func onAdd() {
        let accountingRecord = AccountingRecord(price: self.price, description: self.description, type: self.type)
        
        self.action(accountingRecord)
    }
}

struct NewAccountingRecordView_Previews: PreviewProvider {
    static var previews: some View {
        NewAccountingRecordView { _ in
            
        }
    }
}
