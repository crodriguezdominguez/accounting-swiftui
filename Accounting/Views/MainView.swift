//
//  MainView.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 08/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI
import Combine

struct MainView: View {
    @EnvironmentObject var repository: AccountingRepository
    
    @State var showAddItem = false
    
    var body: some View {
        ZStack {
            NavigationView {
                List {
                    ForEach(self.repository.yearlyAccountings, id: \.year) { item in
                        YearlyAccountingRow(yearlyAccounting: item)
                    }
                    .onDelete(perform: removeItem)
                    .onMove(perform: moveItem)
                }
                .navigationBarTitle("Accounting")
                .navigationBarItems(
                    leading: EditButton(),
                    trailing: Button(action: toggleAlertAddItem, label: { Image(systemName: "plus") }))
            }
            .disabled(showAddItem)
            .blur(radius: showAddItem ? 4.0 : 0.0)
            
            if showAddItem {
                NewYearlyAccountingView(action: onNewYear)
                    .padding()
                    .shadow(radius: 5.0)
            }
        }
    }
    
    func onNewYear(year: Int?) {
        self.toggleAlertAddItem()
        
        if let year = year {
            self.addItem(year: year)
        }
    }
    
    func toggleAlertAddItem() {
        withAnimation(.easeInOut(duration: 0.1)) {
            self.showAddItem.toggle()
        }
    }
    
    func addItem(year: Int) {
        withAnimation {
            let newAccounting = YearlyAccounting(year: year)
            repository.yearlyAccountings.append(newAccounting)
            repository.yearlyAccountings.sort(by: { $0.year > $1.year })
        }
    }
    
    func removeItem(at indices: IndexSet) {
        withAnimation {
            repository.yearlyAccountings.remove(atOffsets: indices)
        }
    }
    
    func moveItem(from source: IndexSet, to destination: Int) {
        repository.yearlyAccountings.move(fromOffsets: source, toOffset: destination)
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            MainView()
                .colorScheme(.light)
        .environmentObject(AccountingRepository.previewData)
            
            MainView()
                .colorScheme(.dark)
        .environmentObject(AccountingRepository.previewData)
            
            MainView()
                .environment(\.locale, Locale(identifier: "es-ES"))
            .environmentObject(AccountingRepository.previewData)
        }
    }
}
