//
//  YearlyAccounting.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 08/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import Foundation

struct YearlyAccounting: Codable, CurrencyConvertible {
    let year: Int
    var monthlyAccountings: [MonthlyAccounting]
    
    init(year: Int) {
        self.year = year
        self.monthlyAccountings = Month.allCases.map{ MonthlyAccounting(month: $0, year: year) }
    }
    
    var total: Float {
        monthlyAccountings.reduce(0.0) { (total, contabilidadMensual) in
            total + contabilidadMensual.total
        }
    }
}

