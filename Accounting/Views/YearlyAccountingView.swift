//
//  YearlyAccountingView.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 08/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct YearlyAccountingView: View {
    @EnvironmentObject var repository: AccountingRepository
    
    let year: Int
    
    var body: some View {
        List(repository.monthlyAccountings(year: year), id: \.month) { monthlyAccounting in
            MonthlyAccountingRow(year: self.year, monthlyAccounting: monthlyAccounting)
        }
        .navigationBarTitle(year.description)
    }
}

struct YearlyAccountingView_Previews: PreviewProvider {
    static var previews: some View {
        return Group {
            NavigationView {
                YearlyAccountingView(year: 2019)
                    .environmentObject(AccountingRepository.previewData)
            }
        }
    }
}
