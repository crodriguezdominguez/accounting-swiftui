//
//  MonthlyAccounting.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 08/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import Foundation

struct MonthlyAccounting: Codable, CurrencyConvertible {
    let month: Month
    var dailyAccountings: [DailyAccounting] = []
    
    init(month: Month, year: Int) {
        self.month = month
        let dias = month.countDays(year: year)
        
        for i in 1...dias {
            dailyAccountings.append(DailyAccounting(day: i))
        }
    }
    
    var total: Float {
        dailyAccountings.reduce(0.0) { (total, contabilidadDiaria) in
            total + contabilidadDiaria.total
        }
    }
}
