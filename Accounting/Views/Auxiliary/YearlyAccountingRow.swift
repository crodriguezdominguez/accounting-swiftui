//
//  YearlyAccountingRow.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 08/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct YearlyAccountingRow: View {
    let yearlyAccounting: YearlyAccounting
    
    var body: some View {
        NavigationLink(destination: YearlyAccountingView(year: yearlyAccounting.year)) {
            Text(yearlyAccounting.year.description)
            
            Spacer()
            
            PriceText(value: yearlyAccounting)
        }
    }
}
struct YearlyAccountingRow_Previews: PreviewProvider {
    @State static var yearly = YearlyAccounting(year: 2019)
    
    static var previews: some View {
        return Group {
            List {
                YearlyAccountingRow(yearlyAccounting: yearly)
            }
            
            List {
                YearlyAccountingRow(yearlyAccounting: yearly)
            }
        }
    }
}
