//
//  MonthlyAccountingRow.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 08/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct MonthlyAccountingRow: View {
    let year: Int
    let monthlyAccounting: MonthlyAccounting
    
    var body: some View {
        NavigationLink(destination: MonthlyAccountingView(year: year, month: monthlyAccounting.month)) {
            Text(monthlyAccounting.month.description)
            
            Spacer()
            
            PriceText(value: monthlyAccounting)
        }
    }
}

struct ContabilidadMensualRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            MonthlyAccountingRow(year: 2019, monthlyAccounting: MonthlyAccounting(month: .february, year: 2019))
        }
    }
}
