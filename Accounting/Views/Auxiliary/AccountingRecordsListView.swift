//
//  AccountingRecordsListView.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 14/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import SwiftUI

struct AccountingRecordsListView: View {
    @EnvironmentObject var repository: AccountingRepository
    
    @Binding var selectedDay: Int
    
    let year: Int
    let month: Month
    
    private var accountingRecords: [AccountingRecord] {
        self.repository.accountingRecords(year: year, month: month, day: selectedDay)
    }
    
    private var dailyAccounting: DailyAccounting {
        self.repository.dailyAccounting(year: year, month: month, day: selectedDay)
    }
    
    var body: some View {
        VStack {
            List {
                ForEach(self.accountingRecords) { record in
                    AccountingRecordRow(accountingRecord: record)
                }
                .onDelete(perform: removeItem)
            }
            
            Text(self.dailyAccounting.description)
                .font(.title)
                .bold()
                .foregroundColor(self.dailyAccounting.total < 0 ? .red : nil)
        }
    }
    
    func removeItem(at indices: IndexSet) {
        withAnimation {
            self.repository.removeAccountingRecords(year: year, month: month, day: selectedDay, at: indices)
        }
    }
}

struct AccountingRecordsListView_Previews : PreviewProvider {
    @State static var day = 0
    
    static var previews: some View {
        Group {
            AccountingRecordsListView(selectedDay: $day, year: 2019, month: .january)
                .environmentObject(AccountingRepository.previewData)
        }
    }
}
