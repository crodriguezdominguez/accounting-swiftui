//
//  AccountingRepository.swift
//  Accounting
//
//  Created by Carlos Rodríguez Domínguez on 08/10/2019.
//  Copyright © 2019 Everyware. All rights reserved.
//

import Foundation

class AccountingRepository: ObservableObject {
    @Published var yearlyAccountings: [YearlyAccounting] {
        didSet {
            if let data = try? JSONEncoder().encode(self.yearlyAccountings) {
                let _ = try? data.write(to: filePath)
            }
        }
    }
    
    private let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("accounting.json")
    
    init() {
        if FileManager.default.fileExists(atPath: filePath.path) {
            do {
                let data = try Data(contentsOf: filePath)
                self.yearlyAccountings = try JSONDecoder().decode([YearlyAccounting].self, from: data)
            } catch _ {
                self.yearlyAccountings = []
            }
        }
        else {
            self.yearlyAccountings = []
        }
    }
    
    init(yearlyAccountings: [YearlyAccounting]) {
        self.yearlyAccountings = yearlyAccountings
    }
    
    var allYears: [Int] {
        yearlyAccountings.map { $0.year }
    }
    
    func indexOf(year: Int) -> Int {
        self.yearlyAccountings.firstIndex { $0.year == year }!
    }
    
    func indexOf(month: Month, year: Int) -> Int {
        self.yearlyAccountings[self.indexOf(year: year)].monthlyAccountings.firstIndex { $0.month == month }!
    }
    
    func monthlyAccountings(year: Int) -> [MonthlyAccounting] {
        self.yearlyAccountings[self.indexOf(year: year)].monthlyAccountings
    }
    
    func monthlyAccounting(year: Int, month: Month) -> MonthlyAccounting {
        self.monthlyAccountings(year: year)[self.indexOf(month: month, year: year)]
    }
    
    func dailyAccounting(year: Int, month: Month, day: Int) -> DailyAccounting {
        let monthlyAccounting = self.monthlyAccounting(year: year, month: month)
        return monthlyAccounting.dailyAccountings[day]
    }
    
    func accountingRecords(year: Int, month: Month, day: Int) -> [AccountingRecord] {
        dailyAccounting(year: year, month: month, day: day).accountingRecords
    }
    
    func removeAccountingRecords(year: Int, month: Month, day: Int, at offsets: IndexSet) {
        self.yearlyAccountings[self.indexOf(year: year)]
            .monthlyAccountings[self.indexOf(month: month, year: year)]
            .dailyAccountings[day]
            .accountingRecords.remove(atOffsets: offsets)
    }
    
    func append(accountingRecord: AccountingRecord, year: Int, month: Month, day: Int) {
        self.yearlyAccountings[self.indexOf(year: year)]
            .monthlyAccountings[self.indexOf(month: month, year: year)]
            .dailyAccountings[day]
            .accountingRecords.append(accountingRecord)
    }
    
    static var previewData: AccountingRepository {
        var accounting = YearlyAccounting(year: 2019)
        accounting.monthlyAccountings[0].dailyAccountings[0].accountingRecords.append(AccountingRecord(price: 23.0, description: "Shoes", type: .expense))
        accounting.monthlyAccountings[0].dailyAccountings[0].accountingRecords.append(AccountingRecord(price: 8.0, description: "Company", type: .profit))
        
        return AccountingRepository(yearlyAccountings: [accounting, YearlyAccounting(year: 2018)])
    }
}
